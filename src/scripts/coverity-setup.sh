if [[ $EUID == 0 ]]; then export SUDO=""; else export SUDO="sudo"; fi

# download polaris binary
curl "$POLARIS_SERVER_URL"/api/tools/v2/downloads/polaris_cli-linux64.zip -L -o polaris.zip
# unzup downloaded file
unzip polaris.zip
# move polaris binary to /usr/bin/
$SUDO cp polaris*/bin/polaris /usr/bin/polaris
