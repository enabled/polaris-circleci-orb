# Changelog
All notable changes to this project will be documented in this file

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
 - Current development changes [ to be moved to release ]
### Changed
 - Current development changes [ to be moved to release ]
### Removed
 - Current development changes [ to be moved to release ]

## [1.0.5] - 2021-12-15
 - Fixed coverity build command parameters

## [1.0.4] - 2021-12-15
 - Added support for ignore capture failure
 - Added support for capture build command

## [1.0.3] - 2021-11-18
 - Fixed typo in `coverity-scan` command

## [1.0.2] - 2021-11-18
 - Fixed typo in `coverity-scan` command

## [1.0.1] - 2021-11-18
 - Updated `coverity` job to accept parameters
 - Updated `coverity-scan` command to accept parameters
 - Rename `default` executor into `cimg`

## [1.0.0] - 2021-11-16
 - Added `coverity` job
