# Synopsys Polaris Orb

A simple CircleCI Orb to run Polaris Coverity scans somewhat easy. Check out [example.yml](./src/examples/example.yml) for usage.

Configuration file `<project_root>/polaris.yml` is optional, the command `polaris/coverity-scan` will generate and use a generic config when it is absent.

The command `polaris/coverity-scan` will always use `<circle_organisation_name>/<circle_repo_name>` as the Polaris project name, the project name in the `polaris.yml` file will be ignored.

# Required Environment Variables

- `POLARIS_ACCESS_TOKEN`
    - Your Polaris service account token
- `POLARIS_SERVER_URL`
    - Your Polaris server URL including the protocal e.g. `https://SUBDOMAIN.polaris.synopsys.com`

# Developer Notes

- Commit subject should always contain the text `[semver:patch|minor|major|skip]`
    - CircleCI will manage version numbers for us, for developments always use `[semver:skip]` so the pipeline will not releast it to public
- If you choose to include a command in the integration test, make sure there's a suscessful dev build exist, otherwise the command will not be found.
